module DispersionCurves

using SpecialFunctions

export Z
export find_zeros, continue_zero

# The plasma dispersion function
Z(ζ) = im * √π * erfcx(-im * ζ)

Base.eps(::Type{Complex{T}}) where T = Base.eps(T)

include("curve.jl")
include("zeros.jl")

end # module DispersionCurves
