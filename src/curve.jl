"""
    continue_zero(D, ω0, k0, kend; steps, tol)

Use continuation to solve

    D(ω_i, k_i) = 0, for i ∈ 1:steps

where ω0 and k0 are such that D(ω0, k0) = 0

# Examples
```julia-repl
julia> ps, xs = continue_zero((p, x) -> x^2 - p, 1., 1., 10.)
julia> all(xs.^2 == ps)
true
```
"""
function continue_zero(D::Function, ω0::W, k0::T, kend::T; steps::Int=100,
    tol::Number=√eps(T),
    forwardmap=identity, inversemap=identity) where {W<:Number, T<:Number}

    ks = inversemap.(range(forwardmap(k0), forwardmap(kend), steps + 1))

    ωs = zeros(W, steps + 1)
    ωs[1] = ω0

    for step ∈ 1:steps
        if step == 1
            guess = ωs[step]
        else
            guess = ωs[step] + (ks[step + 1] - ks[step]) *
                (ωs[step] - ωs[step - 1]) / (ks[step] - ks[step - 1])
        end
        ωs[step + 1], success = newton(x -> D(x, ks[step + 1]), guess, tol=tol)

        if !success
            println("Newton failed during continuation...")
        end
    end

    return ωs, ks
end

"""
    continue_zero(D, ω0, kneg, k0, kpos; steps, tol)

Use continuation in both directions to solve

    D(ω_i, k_i) = 0, for i ∈ 1:steps

where ω0 and k0 are such that D(ω0, k0) = 0, and k0 ∈ (kneg, kpos)

# Examples
```julia-repl
julia> ps, xs = continue_zero((p, x) -> x^2 - p, 1., 0., 1., 10.)
julia> all(xs.^2 == ps)
true
```
"""
function continue_zero(D::Function, ω0::W, kneg::T, k0::T, kpos::T; steps::Int=100,
    tol::Number=√eps(T),
    forwardmap=identity, inversemap=identity) where {W<:Number, T<:Number}

    knegm = forwardmap(kneg)
    k0m = forwardmap(k0)
    kposm = forwardmap(kpos)

    stepsneg = ceil(Int, steps * abs((k0m - knegm) / (kposm - knegm)))
    stepspos = ceil(Int, steps * abs((kposm - k0m) / (kposm - knegm)))

    ωs_neg, ks_neg = continue_zero(D, ω0, k0, kneg, steps=stepsneg, tol=tol,
        forwardmap=forwardmap, inversemap=inversemap)
    ωs_pos, ks_pos = continue_zero(D, ω0, k0, kpos, steps=stepspos, tol=tol,
        forwardmap=forwardmap, inversemap=inversemap)

    ksm = vcat(reverse(ks_neg), ks_pos)
    ωs = vcat(reverse(ωs_neg), ωs_pos)

    return ωs, ksm
end
