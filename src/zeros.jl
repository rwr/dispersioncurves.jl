using RootsAndPoles
using Zygote

function find_zeros(f, zmin, zmax; rtol=√eps(), dz=abs(zmax - zmin) / 100)
    triangulation = rectangulardomain(zmin, zmax, dz)
    zroots, ~ = grpf(f, triangulation, GRPFParams(1000, rtol))
    return zroots
end


function jacobi(f::Function, x::T) where T<: Complex
    fx, back = Zygote.pullback(f, x)
    return fx, back(1)[1], back(im)[1]
end

function ∇(f::Function, x::T) where T <: Complex
    fx, du, dv = jacobi(f, x)
    return (du' + im * dv') / 2
end

∇(f, x::T) where T <: Real = gradient(f, x)[1]

function newton(f::Function, x0::T; tol=√eps(T), maxiter::Int=16) where T
    success = false

    for _ ∈ 1:maxiter
        f0 = f(x0)
        success = abs(f0) < tol
        if success break end

        df0 = ∇(f, x0)
        if abs(df0) == 0 break end

        dx = f0 / df0
        x0 -= dx

        success = abs(dx) < tol
        if success break end
    end

    return x0, success
end
