# DispersionCurves.jl
A small package for plotting dispersion curves $(\omega(k), k)$ which satisfy $D(\omega(k), k) = 0$ for some function $D$. 

## Installation
Installation is done as follows:
   - Launch julia and enter the following: `] add https://gitlab.mpcdf.mpg.de/rwr/dispersioncurves.jl.git`
   - The examples require some additional dependencies, they can be installed using: `] add LaTeXStrings, Plots, CSV, DataFrames`
   - For running the Jupyter notebooks an additional package is required: `] add IJulia`; notebooks can then be opened using `using IJulia; notebook()`, which should open the Jupyter notebook browser page on the default web browser.

## Examples
Example Jupyter notebooks are given in `examples/`.

