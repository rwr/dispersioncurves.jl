using Plots, Images, ImageTransformations, LaTeXStrings

# See https://discourse.julialang.org/t/how-to-set-ticklabels-for-an-image-within-the-range-of-x-and-y/63637
function perfract(x, t, m=0.7, M=1)
  x = x / t
  return m + (M - m) * (x-floor(x))
end

function domcol(w; n=10)
  logm = log.(abs.(w))        # for lines of constant modulus
  H = angle.(w) * 180 / π     # compute argument of  w within interval [-180, 180], iei the Hue

  V = perfract.(logm, 2π / n) # lines of constant log-modulus
  arr = permutedims(cat(H, ones(size(H)), V, dims = 3), [3, 1, 2]) # HSV-array

  return RGB.(colorview(HSV, arr))
end

function plot_disp(f, z0, z1; roots=Nothing, n=200)
  x = range(z0.re, z1.re, length=n)
  y = range(z0.im, z1.im, length=n)
  z = [f(u + im * v) for v in y, u in x]

  plt = plot(x, y, domcol(z), xlims=(x[1], x[end]), ylims=(y[1], y[end]), aspect_ratio=(x[end] - x[1])/(y[end] - y[1]))

  if roots != Nothing
    scatter!(real(roots), imag(roots), markercolor=:white, legend=false)
  end

  plot!(xlabel=L"\Re(\bar\omega)", ylabel=L"\Im(\bar\omega)")

  return plt
end
