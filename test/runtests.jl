using DispersionCurves
using Test
using CSV, DataFrames

@testset "DispersionCurves.jl" begin
    # Simple exmples
    Ts = [Float16, Float32, Float64]
    fs = [(p, x) -> p^2 - x, (p, x) -> sin(p - 1) - (x - 1)]
    for T ∈ Ts
        for f ∈ fs
            tol = √eps(T)
            ps, xs = continue_zero(f, 1., 1., 2.; steps=100, tol=tol)
            @test all(abs.(f.(ps, xs)) .≤ 2tol)

            ps, xs = continue_zero(f, 1., 1E-3, 1., 2.; steps=100, tol=tol)
            @test all(abs.(f.(ps, xs)) .≤ 2tol)

            ps, xs = continue_zero(f, 1., 1E-3, 1., 2.; steps=100, tol=tol,
                forwardmap=log, inversemap=exp)

            @test all(abs.(f.(ps, xs)) .≤ 2tol)
        end
    end

    # Alfven wave
    μ, kperp, β0 = 1/3670.5, 2E-4, 0.1
    D(ω, β) = kperp^2 - 2(β * ω^2 - μ) * (1 + ω * Z(ω))

    # Initial roots
    z0, z1 = 0.04 - 2E-2im, 0.06 + 2E-2im
    ω0s = find_zeros(z -> D(z, β0), z0, z1)
    ω0s[1], ~ = DispersionCurves.newton(z -> D(z, β0), ω0s[1]; tol=eps())
    @test length(ω0s) == 1
    @test isapprox(real(ω0s[1]), 0.052, rtol=1E-2)
    @test isapprox(imag(ω0s[1]), -1.77e-7, rtol=1E-2)

    # Varying β
    ωs, βs = reverse.(continue_zero(D, ω0s[1], β0, 0.01, steps=10000))


    # Reference result
    ref_re = CSV.read("examples/data/Kleiber_PoP2016_fig2a.csv", DataFrame)
    ref_re[!, 2] /= √2
    ref_im = CSV.read("examples/data/Kleiber_PoP2016_fig2c.csv", DataFrame)

    function interpolate_to_refβ(ωs, βs, ref_βs)
        ref_ωs = zeros(size(ref_βs))
        for (rdx, β) ∈ enumerate(ref_βs)
            idx = findfirst(x -> x > β, βs)
            if isempty(idx)
                ref_ωs[rdx] = real(ωs[end])
            elseif idx == firstindex(βs)
                ref_ωs[rdx] = real(ωs[idx])
            else
                β0, β1 = βs[(idx - 1):idx]
                ω0, ω1 = real(ωs[(idx - 1):idx])
                ref_ωs[rdx] = ω0 + (ω1 - ω0) * (β - β0) / (β1 - β0)
            end
        end

        return ref_ωs
    end

    # Interpolate to reference k values
    ωs_re = interpolate_to_refβ(real(ωs), βs, ref_re[!, 1])
    ωs_im = interpolate_to_refβ(imag(ωs), βs, ref_im[!, 1])

    @test isapprox(ωs_re, ref_re[!, 2], rtol=1E-3)
    @test isapprox(ωs_im, ref_im[!, 2], rtol=1E-2)
end
